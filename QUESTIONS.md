## Beschreibe dein Projekt kurz.

## Welches gesellschaftliches Problem willst du mit deinem Projekt lösen?

## Wie willst du dein Projekt technisch umsetzen?

## Welche ähnlichen Lösungen gibt es schon und was wird dein Projekt anders bzw. besser machen?

## Wer ist die Zielgruppe und wie soll dein Tool sie erreichen?

## Hast du schon an der Idee gearbeitet? Wenn ja, beschreibe kurz den aktuellen Stand und erkläre die Neuerung.

## Wie viele Stunden willst du (bzw. will das Team) insgesamt in den 6 Monaten Förderzeitraum an der Umsetzung arbeiten?

*Max. 950 h*

*<Stunden pro Woche pro Person>* x *<Anzahl der Teammitglieder>* x 26 =

## Skizziere kurz die wichtigsten Meilensteine, die du (bzw. das Team) im Förderzeitraum umsetzen willst.

## Zusätzlich:
- GitHub/GitLab/Codeberg/Cgit/etc.
